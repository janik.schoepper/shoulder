# Importing Required libraries & Modules
from tkinter import *
from tkinter import messagebox
from tkinter import filedialog
from tkinter import ttk
import os
import time
from PIL import ImageTk, Image
from spellchecker import SpellChecker
from textEditor import TextEditor


# Creating TK Container
root = Tk()

img = ImageTk.PhotoImage(Image.open("D:\git\HoHCI\shoulder\img\small-a-letter-a-logo-icon-element-design-symbol-vector-8142337.jpg"))
#panel = Label(root, image = img)
#panel.pack(side = "right")

# Passing Root to TextEditor Class
test = TextEditor(root)
test.insertText('Beispiel Text is da, aber nicht heir. test ist doof, so ist nun mal das Leben')
event = []

def return_pressed(event):
    root.after(5000, return_pressed2)
    #test.retrieve_input()
def return_pressed2():
    test.retrieve_input()
def log( event):
    print(event)

def check_spelling():
    test.check_correct_spelling()
    root.after(6000,check_spelling)

# Root Window Looping
"""
root.bind('<.>', log, add='+')
root.bind('<.>', return_pressed)
root.bind('<,>', return_pressed)
root.bind('<:>', return_pressed)
root.bind('<?>', return_pressed)
root.bind('<!>', return_pressed)
"""
#root.bind('<space>', return_pressed)
#root.after(5000, check_spelling)

root.mainloop()