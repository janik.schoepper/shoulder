# Importing Required libraries & Modules
from tkinter import *
from tkinter import messagebox
from tkinter import filedialog
from tkinter import ttk
import os
import re
import time
from PIL import ImageTk, Image
from spellchecker import SpellChecker
import re
import language_tool_python as lt
import random
import string



spell = SpellChecker(language='de')
tool = lt.LanguageToolPublicAPI('de-DE')
tool.close()


def get_random_string(length):
    # choose from all lowercase letter
    letters = string.ascii_lowercase
    result_str = ''.join(random.choice(letters) for i in range(length))
    print("Random string of length", length, "is:", result_str)
    return result_str


# Defining TextEditor Class
class TextEditor:
    # Defining Constructor
    def __init__(self,root):
        # Assigning root
        self.root = root
        # Title of the window
        self.root.title("TEXT EDITOR")
        # Window Geometry
        self.root.geometry("1200x700+200+150")
        # Initializing filename
        self.filename = None
        # Declaring Title variable
        self.title = StringVar()
        # Declaring Title variable
        self.comment = StringVar()
        # Declaring Status variable
        self.status = StringVar()
        # Creating Titlebar
        self.titlebar = Label(self.root,textvariable=self.title,font=("times new roman",15,"bold"),bd=2,relief=GROOVE)
        # Packing Titlebar to root window
        self.titlebar.pack(side=TOP,fill=BOTH)
        # Calling Settitle Function
        self.settitle()
        # Creating Commentbar
        self.commentbar = Label(self.root,textvariable=self.comment,font=("times new roman",15,"bold"),bd=2,relief=GROOVE)
        # Packing Commentbar to root window
        self.commentbar.pack(side=RIGHT,fill=BOTH)
        # Creating Statusbar
        self.statusbar = Label(self.root,textvariable=self.status,font=("times new roman",15,"bold"),bd=2,relief=GROOVE)
        # Packing status bar to root window
        self.statusbar.pack(side=BOTTOM,fill=BOTH)
        # Initializing Status
        self.status.set("Welcome To Text Editor")
        # Creating Menubar
        self.menubar = Menu(self.root,font=("times new roman",15,"bold"),activebackground="skyblue")
        # Configuring menubar on root window
        self.root.config(menu=self.menubar)
        # Creating File Menu
        self.filemenu = Menu(self.menubar,font=("times new roman",12,"bold"),activebackground="skyblue",tearoff=0)
        # Adding New file Command
        self.filemenu.add_command(label="New",accelerator="Ctrl+N",command=self.newfile)
        # Adding Open file Command
        self.filemenu.add_command(label="Open",accelerator="Ctrl+O",command=self.openfile)
        # Adding Save File Command
        self.filemenu.add_command(label="Save",accelerator="Ctrl+S",command=self.savefile)
        # Adding Save As file Command
        self.filemenu.add_command(label="Save As",accelerator="Ctrl+A",command=self.saveasfile)
        # Adding Seprator
        self.filemenu.add_separator()
        # Adding Exit window Command
        self.filemenu.add_command(label="Exit",accelerator="Ctrl+E",command=self.exit)
        # Cascading filemenu to menubar
        self.menubar.add_cascade(label="File", menu=self.filemenu)
        # Creating Edit Menu
        self.editmenu = Menu(self.menubar,font=("times new roman",12,"bold"),activebackground="skyblue",tearoff=0)
        # Adding Cut text Command
        self.editmenu.add_command(label="Cut",accelerator="Ctrl+X",command=self.cut)
        # Adding Copy text Command
        self.editmenu.add_command(label="Copy",accelerator="Ctrl+C",command=self.copy)
        # Adding Paste text command
        self.editmenu.add_command(label="Paste",accelerator="Ctrl+V",command=self.paste)
        # Adding Seprator
        self.editmenu.add_separator()
        # Adding Undo text Command
        self.editmenu.add_command(label="Undo",accelerator="Ctrl+U",command=self.undo)
        # Cascading editmenu to menubar
        self.menubar.add_cascade(label="Edit", menu=self.editmenu)
        # Creating Help Menu
        self.helpmenu = Menu(self.menubar,font=("times new roman",12,"bold"),activebackground="skyblue",tearoff=0)
        # Adding About Command
        self.helpmenu.add_command(label="About",command=self.infoabout)
        # Cascading helpmenu to menubar
        self.menubar.add_cascade(label="Help", menu=self.helpmenu)
        # Creating Scrollbar
        scrol_y = Scrollbar(self.root,orient=VERTICAL)
        # Creating Text Area
        self.txtarea = Text(self.root, yscrollcommand=scrol_y.set,font=("times new roman",15,"bold"),state="normal",relief=GROOVE, wrap=WORD)
        self.txtarea.config(pady=10, padx=40)
        # Packing scrollbar to root window
        scrol_y.pack(side=RIGHT,fill=Y)
        # Adding Scrollbar to text area
        scrol_y.config(command=self.txtarea.yview)
        # Packing Text Area to root window

        self.txtarea.pack(fill=BOTH,expand=1)

        # BUTTON
        self.button = Button(self.root,text='Get Feedback',font=('normal',10),command= lambda: self.retrieve_input_button(), bg='yellow') #        
        self.button.place(x=270,y=520)
        self.button.pack()
        #self.button.pack_forget()

        self.tipwindow = None
        
        # Calling shortcuts funtion
        self.shortcuts()
  
    # Defining settitle function
    def settitle(self):
        # Checking if Filename is not None
        if self.filename:
            # Updating Title as filename
            self.title.set(self.filename)
        else:
            # Updating Title as Untitled
            self.title.set("Untitled")
    # Defining setcomment function
    def setcomment(self, text):
        self.comment.set(self.comment.get()+ '\n' + text)
    def showComment(self):
        print('HEYYY')

    def showtip(self, text):
        "Display text in tooltip window"
        self.text = text
        if self.tipwindow or not self.text:
            return
        x, y, cx, cy = self.txtarea.bbox("current")
        x = x + self.txtarea.winfo_rootx() + 57
        y = y + cy + self.txtarea.winfo_rooty() +27
        self.tipwindow = tw = Toplevel(self.txtarea)
        tw.wm_overrideredirect(1)
        tw.wm_geometry("+%d+%d" % (x, y))
        label = Label(tw, text=self.text, justify=LEFT,
                      background="#ffffe0", relief=SOLID, borderwidth=1,
                      font=("tahoma", "8", "normal"))
        label.pack(ipadx=1)

    def hidetip(self):
        tw = self.tipwindow
        self.tipwindow = None
        if tw:
            tw.destroy()
    def enter(self, event, text):
        self.showtip(text)
    def leave(self, event):
        self.hidetip()            
    # Defining New file Function
    def newfile(self,*args):
        # Clearing the Text Area
        self.txtarea.delete("1.0",END)
        # Updating filename as None
        self.filename = None
        # Calling settitle funtion
        self.settitle()
        # updating status
        self.status.set("New File Created")
    # Defining Open File Funtion
    def openfile(self,*args):
        # Exception handling
        try:
            # Asking for file to open
            self.filename = filedialog.askopenfilename(title = "Select file",filetypes = (("All Files","*.*"),("Text Files","*.txt"),("Python Files","*.py")))
            # checking if filename not none
            if self.filename:
                # opening file in readmode
                infile = open(self.filename,"r")
                # Clearing text area
                self.txtarea.delete("1.0",END)
                # Inserting data Line by line into text area
                for line in infile:
                    self.txtarea.insert(END,line)
                # Closing the file  
                infile.close()
                # Calling Set title
                self.settitle()
                # Updating Status
                self.status.set("Opened Successfully")
        except Exception as e:
            messagebox.showerror("Exception",e)
    # Defining Save File Funtion
    def savefile(self,*args):
        # Exception handling
        try:
        # checking if filename not none
            if self.filename:
                # Reading the data from text area
                data = self.txtarea.get("1.0",END)
                # opening File in write mode
                outfile = open(self.filename,"w")
                # Writing Data into file
                outfile.write(data)
                # Closing File
                outfile.close()
                # Calling Set title
                self.settitle()
                # Updating Status
                self.status.set("Saved Successfully")
            else:
                self.saveasfile()
        except Exception as e:
            messagebox.showerror("Exception",e)
    # Defining Save As File Funtion
    def saveasfile(self,*args):
        # Exception handling
        try:
            # Asking for file name and type to save
            untitledfile = filedialog.asksaveasfilename(title = "Save file As",defaultextension=".txt",initialfile = "Untitled.txt",filetypes = (("All Files","*.*"),("Text Files","*.txt"),("Python Files","*.py")))
            # Reading the data from text area
            data = self.txtarea.get("1.0",END)
            # opening File in write mode
            outfile = open(untitledfile,"w")
            # Writing Data into file
            outfile.write(data)
            # Closing File
            outfile.close()
            # Updating filename as Untitled
            self.filename = untitledfile
            # Calling Set title
            self.settitle()
            # Updating Status
            self.status.set("Saved Successfully")
        except Exception as e:
            messagebox.showerror("Exception",e)
    # Defining Exit Funtion
    def exit(self,*args):
        op = messagebox.askyesno("WARNING","Your Unsaved Data May be Lost!!")
        if op>0:
            self.root.destroy()
        else:
            return
    # Defining Cut Funtion
    def cut(self,*args):
        self.txtarea.event_generate("<<Cut>>")
    # Defining Copy Funtion
    def copy(self,*args):
            self.txtarea.event_generate("<<Copy>>")
    # Defining Paste Funtion
    def paste(self,*args):
        self.txtarea.event_generate("<<Paste>>")
    # Defining Undo Funtion
    def undo(self,*args):
        # Exception handling
        try:
            # checking if filename not none
            if self.filename:
                # Clearing Text Area
                self.txtarea.delete("1.0",END)
                # opening File in read mode
                infile = open(self.filename,"r")
                # Inserting data Line by line into text area
                for line in infile:
                    self.txtarea.insert(END,line)
                # Closing File
                infile.close()
                # Calling Set title
                self.settitle()
                # Updating Status
                self.status.set("Undone Successfully")
            else:
                # Clearing Text Area
                self.txtarea.delete("1.0",END)
                # Updating filename as None
                self.filename = None
                # Calling Set title
                self.settitle()
                # Updating Status
                self.status.set("Undone Successfully")
        except Exception as e:
            messagebox.showerror("Exception",e)
   
   
    # Defining About Funtion
    def infoabout(self):
        messagebox.showinfo("About Text Editor","A Simple Text Editor\nCreated using Python.")
    def insertText(self, text):
        self.txtarea.insert(INSERT, text)  
        self.txtarea.pack(expand= 1, fill= BOTH)
    def deleteWord(self, text):
        offset = '+%dc' % len(text) # +5c (5 chars)
        self.txtarea.delete(INSERT, INSERT+offset)    
        self.txtarea.pack(expand= 1, fill= BOTH)
    def set_value(self, input):
        self.txtarea.delete(1.0,END)
        self.txtarea.insert("end-1c", input)     
        self.txtarea.pack(expand= 1, fill= BOTH)
    def insertTextDelay(self, text):
        self.root.after(10000, lambda: self.txtarea.insert(INSERT, text))       
        self.txtarea.pack(expand= 1, fill= BOTH)

    def check_marked(self, word):
        # word length use as offset to get end position for tag
        offset = '+%dc' % len(word) # +5c (5 chars)

        # search word from first char (1.0) to the end of text (END)
        pos_start = self.txtarea.search(word, '1.0', END)

        # create end position by adding (as string "+5c") number of chars in searched word 
        pos_end = pos_start + offset

        print ('position: '+str(pos_start) + ' ' + str(pos_end)) # 1.6 1.6+5c :for first `World`
        #print(f'Tag Ranges: {len(self.txtarea.tag_ranges(word))}')
        print(f'Tag Names: {self.txtarea.tag_names()}')
        tagList = self.txtarea.tag_names()
        for tag in tagList:
            if(len(self.txtarea.tag_nextrange(tag, pos_start,pos_end))>0):
                self.txtarea.tag_delete(tag)
        if (len(self.txtarea.tag_ranges(word))>0):
            print('found tag!!')
            #return True
        #else:
            #return False
        pos_start = self.txtarea.search(word, pos_end, END)   

    def correct_word(self,word):
        self.deleteWord(word)
        self.insertText(tool.check(word)[0].replacements[0])

    def removeTag(self, word, pos_start, pos_end):
        """
        # word length use as offset to get end position for tag
        offset = '+%dc' % len(word) # +5c (5 chars)
        inputOG = self.txtarea.get("1.0",'end-1c')
        count = inputOG.count(word)
        # search word from first char (1.0) to the end of text (END)
        pos_start = self.txtarea.search(word, '1.0', END)
        for i in range(count):
            # create end position by adding (as string "+5c") number of chars in searched word 
            pos_end = pos_start + offset
        """

        self.txtarea.tag_delete(word)
            #return True
        #else:
            #return False
        pos_start = self.txtarea.search(word, pos_end, END) 

    def markSpelling(self, word, pos_start, infoText, type):
        # word length use as offset to get end position for tag
        offset = '+%dc' % len(word) # +5c (5 chars)

        # check if found the word
        #while pos_start:

        # create end position by adding (as string "+5c") number of chars in searched word 
        print(f'START: {pos_start}')
        pos_end = pos_start + offset
        randomStr = get_random_string(10)
        # add tag

        if(type=='spelling'):
            self.txtarea.tag_config(randomStr, foreground="red", underline=1)
        else: 
            self.txtarea.tag_config(randomStr, foreground="blue", underline=1)
        self.txtarea.tag_bind(randomStr, '<Enter>',  lambda x: self.showtip(infoText))
        self.txtarea.tag_bind(randomStr, '<Leave>',self.leave)
        self.txtarea.tag_bind(randomStr, '<Double-Button-1>',  lambda x: self.removeTag(randomStr, pos_start, pos_end))
        self.txtarea.tag_add(randomStr, pos_start, pos_end)
        print('Done')

    def check_correct_spelling(self):
        inputOG = self.txtarea.get("1.0",'end-1c')
        inputSplit = inputOG.split(' ')
        for word in inputSplit:
            print(tool.check(word))
            if(not len(tool.check(word))>0):
                print(word)
                self.check_marked(word)
                print(True)


    def retrieve_input(self):

        inputOG = self.txtarea.get("1.0",INSERT)
        res = re.split('\.|\:|\!|\?', inputOG)
        workingText = res[-2]
        print(f'WORKING TEXT: {workingText}')
        """

        inputSplit = inputOG.split(' ')
        for word in inputSplit:
            print(tool.check(word))
            if(not len(tool.check(word))>0):
                print(word)
                self.check_marked(word)
                print(True)
        """
        matches = tool.check(workingText)
        lenRest = len(' '.join(res[:-2]))+1
        print(lenRest)
        print(matches)
        for match in matches:

            word = inputOG[lenRest+match.offset:lenRest+ match.errorLength+match.offset]
            count_word = inputOG[:lenRest+match.offset].count(word)
            pos_end =  '1.0'
            for n in range(count_word+1):
                offset = '+%dc' % len(word)
                # search word from first char (1.0) to the end of text (END)
                pos_start = self.txtarea.search(word, pos_end, END)
                pos_end = pos_start + offset
            print(f'POSSTART: {pos_start}')
            try: 
                match.replacements[0]
            except Exception:
                infoText = f'Word is really weird actually.'
                errType = 'spelling'
                print('No replacemant found')
            else:
            
                if match.ruleIssueType == 'misspelling':
                    infoText = f'Word should be {match.replacements[0]}'
                    errType = 'spelling'
                else:
                    infoText = match.message + '\n Vorgeschlagene Änderungen' + str(match.replacements)
                    errType = 'grammar'

            self.markSpelling(word, pos_start, infoText, errType)
             

    def retrieve_input_button(self):
        inputOG = self.txtarea.get("1.0",'end-1c')
        """

        inputSplit = inputOG.split(' ')
        for word in inputSplit:
            print(tool.check(word))
            if(not len(tool.check(word))>0):
                print(word)
                self.check_marked(word)
                print(True)
        """
        matches = tool.check(inputOG)
        print(matches)
        for match in matches:

            word = inputOG[match.offset:match.errorLength+match.offset]
            count_word = inputOG[:match.offset].count(word)
            pos_end =  '1.0'
            for n in range(count_word+1):
                offset = '+%dc' % len(word)
                # search word from first char (1.0) to the end of text (END)
                pos_start = self.txtarea.search(word, pos_end, END)
                pos_end = pos_start + offset
            print(f'POSSTART: {pos_start}')
            try: 
                match.replacements[0]
            except Exception:
                infoText = f'Word is really weird actually.'
                errType = 'spelling'
                print('No replacemant found')
            else:
            
                if match.ruleIssueType == 'misspelling':
                    infoText = f'Word should be {match.replacements[0]}'
                    errType = 'spelling'
                else:
                    infoText = match.message + '\n Vorgeschlagene Änderungen' + str(match.replacements)
                    errType = 'grammar'
            self.markSpelling(word, pos_start, infoText, errType)


    #def return_pressed(self, event):
    #    print('Return key pressed.')

    #def log(self, event):
    #    print(event)

    # Defining shortcuts Funtion
    def shortcuts(self):
        # Binding Ctrl+n to newfile funtion
        self.txtarea.bind("<Control-n>",self.newfile)
        # Binding Ctrl+o to openfile funtion
        self.txtarea.bind("<Control-o>",self.openfile)
        # Binding Ctrl+s to savefile funtion
        self.txtarea.bind("<Control-s>",self.savefile)
        # Binding Ctrl+a to saveasfile funtion
        self.txtarea.bind("<Control-a>",self.saveasfile)
        # Binding Ctrl+e to exit funtion
        self.txtarea.bind("<Control-e>",self.exit)
        # Binding Ctrl+x to cut funtion
        self.txtarea.bind("<Control-x>",self.cut)
        # Binding Ctrl+c to copy funtion
        self.txtarea.bind("<Control-c>",self.copy)
        # Binding Ctrl+v to paste funtion
        self.txtarea.bind("<Control-v>",self.paste)
        # Binding Ctrl+u to undo funtion
        self.txtarea.bind("<Control-u>",self.undo)



