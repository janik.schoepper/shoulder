import csv
from collections import Counter
import pandas as pd

def readkeylog_total(key,id):
    path = 'C:/Users/User/Documents/Uni/HoHCI/bot/shoulder/data/UT03AN/keylog_UT03AN.csv'
    print(path)
    with open(path) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=';')
        keycounter = 0
        for row in csv_reader:
            if key in row[1]:
                keycounter = keycounter + 1
    print('participant ' + str(id) + ' pressed' + str(key) + ' a total of ' + str(keycounter) + ' times')
    return 'participant ' + str(id) + ' pressed' + str(key) + ' a total of ' + str(keycounter) + ' times'

def readkeylog_time(key,id,start,stop):
    path = 'C:/Users/User/Documents/Uni/HoHCI/bot/shoulder/data/UT03AN/keylog_UT03AN.csv'
    subset = pd.read_csv(path).loc[start:stop]
    keycounter = 0
    for row in subset:
        if key in row[1]:
            keycounter = keycounter + 1
    print('participant ' + str(id) + ' pressed' + str(key) + ' a total of ' + str(keycounter) + ' times')
    return 'participant ' + str(id) + ' pressed' + str(key) + ' a total of ' + str(keycounter) + ' times'

readkeylog_total('Key.backspace','UT03AN')
readkeylog_time('Key.backspace','UT03AN',0,2719)
