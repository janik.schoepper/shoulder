from pynput.mouse import Listener
import logging


logging.basicConfig(filename=("mouselog.csv"), level=logging.DEBUG, format=" %(asctime)s ; %(message)s")

def on_move(x, y):
    logging.info(str(f'Moved; {(x, y)}'))

def on_click(x, y, button, pressed):
    if(pressed):
        logging.info(str(f'Pressed {button}; {(x,y)}'))
    else:
        logging.info(str(f'Released {button}; {(x,y)}'))
    
    """
    if not pressed:
        # Stop listener
        return False
    """


def on_scroll(x, y, dx, dy):
    logging.info(str('Scrolled; {0}'.format((x, y))))



# Collect events until released
with Listener(
        on_move=on_move,
        on_click=on_click,
        on_scroll=on_scroll) as listener:
    listener.join()