function evaluate() {
  
  var activeDocument = DocumentApp.getActiveDocument();
  var docBody = activeDocument.getBody();

  // Other
  Logger.log("Time evaluated: " + new Date().toLocaleString());

  // Gesamter Text ohne Whitespaces am Anfang&Ende
  var text = docBody.getText();

  // Alle Letter in einem String
  var letters = text.toString(); //Text with page breaks
  Logger.log("Full Text length:" + letters.length)

  // Breaks removed
  var letters_breaksReplaced = letters.replace(/(\r\n|\n|\r)/gm, " ").replace(/\s\s+/g, ' ');
  var letters_breaksRemoved = letters.replace(/(\r\n|\n|\r)/gm, "");
  Logger.log("Breaks total:" + (letters.length - letters_breaksRemoved.length));

  var paragraphs = docBody.getParagraphs();
  Logger.log("Number of paragraphs (by Function):" + paragraphs.length); // *Paragraphs + Breaks
  
  // Whitespaces
  var letters_noWhiteSpaces = letters_breaksRemoved.replace(/\s/g, ""); 
  Logger.log("Total letters (No Whitespace):" + letters_noWhiteSpaces.length);
  var whitespaces = letters_breaksRemoved.length - letters_noWhiteSpaces.length;
  Logger.log("Total (Typed) Whitespaces:" + (whitespaces));
  Logger.log("Total words:" + (letters_breaksReplaced.length - letters_noWhiteSpaces.length + 1));

  // Text Logs
  Logger.log("Full Text:\n\n" + letters)
  Logger.log("Breaks replaced by Whitespaces: " + letters_breaksReplaced);
  Logger.log("Breaks removed: " + letters_breaksRemoved);
  Logger.log("Text without Whitespaces: " + letters_noWhiteSpaces)


  // get complete log and save it
  var data = Logger.getLog();

  var Folder = DriveApp.getFolderById("18xp1vEqHSvqTxytGqQ_sC3bkfU958QQ-");
  Folder.createFile("Aufgabe2_Auswertung", data);
}


  //Logger.log("Erster Paragraph:" + paragraphs[0].getText());
  //paragraphs[0].setText("TextSubmission:");
  //paragraphs[0].setAttributes({FONT_SIZE:30, HORIZONTAL_ALIGNMENT: DocumentApp.HorizontalAlignment.LEFT})

  // Attributes of Text
  // var attributes = paragraphs[0].getAttributes();
  // Logger.log(attributes);
  // Set Attributes